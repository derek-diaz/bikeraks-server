// We load the Location model
var BikeRaks = require('../models/bikeraks');
var User = require('../models/user');
var mongoose = require('../node_modules/mongoose');

// Create endpoint /api/locations for POSTS
exports.postLocations = function(req, res) {
 
    // Create a new instance of the Location model
    var bikerak = new BikeRaks();
 
    // Set the location properties that came from the POST data
    console.log(req.body.message);
    console.log(req.body.latitude);
    console.log(req.body.longitude);
 
    bikerak.latitude = req.body.latitude;
    bikerak.longitude = req.body.longitude;
    bikerak.message = req.body.message;
 
    //passport will automatically set the user in req.user
    bikerak.userId = req.user._id;
 
    // Save the location and check for errors
    bikerak.save(function(err) {
        if (err){
            res.send(err);
            return;
        }
 
        res.json({
            success: 'Location added successfully',
            data: bikerak
        });
    });
};

exports.getLocations = function(req, res) {
    // Use the Location model to find all locations
    // from particular user with their username
    BikeRaks.find({}).lean().exec(function(err, bikeraks) {
        if(err){
            res.send(err);
            return;
        }
 
        //We want to set the username on each location by looking
        //up the userId in the User documents.
        //
        //Because of mongoose asynchronism, we will have to wait
        //to get all the results from the queries on the User model
        //We can send them when we have iterated
        //through every location (counter === l)
        var counter = 0;
        var l = bikeraks.length;
 
        //We create a closure to have access to the location
        var closure = function(bikeraks){
 
            return function(err, user){
 
                counter++;
 
                if(err)
                    res.send(err);
 
                bikeraks.username = user.username;
 
                //when all the users have been set
                if(counter === l ) {
                    //respond
                    res.json(bikeraks);
                    return;
                }           
            };
        };
 
        //We iterate through all locations to find their associated
        //username.
        for (var i = 0; i < l; i++) {
            User.findById(bikeraks[i].userId, closure(bikeraks[i]));
        }
    });
};

// Create endpoint /api/locations/:location_id for GET
exports.getLocation = function(req, res) {
    // Use the Location model to find a specific location
    console.log(req.user._id);
    BikeRaks.find({
        userId: req.user._id,
        _id: req.params.location_id
    }, function(err, bikeraks) {
        if (err)
            res.send(err);
 
        res.json(bikeraks);
    });
};

// Create endpoint /api/locations/:location_id for PUT
exports.putLocation = function(req, res) {
    // Use the Location model to find a specific location
    BikeRaks.update({
        userId: req.user._id,
        _id: req.params.location_id
    }, {
        message: req.body.message
    }, function(err, num, raw) {
        if (err)
            res.send(err);
 
        res.json({
            message: 'message updated'
        });
    });
};

// Create endpoint /api/locations/:location_id for DELETE
exports.deleteLocation = function(req, res) {
    // Use the Location model to find a specific location and remove it
    BikeRaks.remove({
        userId: req.user._id,
        _id: req.params.location_id
    }, function(err) {
        if (err)
            res.send(err);
 
        res.json({
            message: 'Location deleted'
        });
    });
};