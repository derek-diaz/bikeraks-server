// Load MongoDB driver
var mongoose = require('mongoose');
 
// Define our location schema
var BikeRakSchema = new mongoose.Schema({
  longitude: Number,
  latitude: Number,
  title: String,
  userId: String
});
 
// We bind the Location model to the LocationSchema
module.exports = mongoose.model('BikeRaks', BikeRakSchema);