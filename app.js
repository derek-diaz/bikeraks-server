//BIKERAKS 2015
//DEREK DIAZ CORREA

//Required Libraries
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');

//Load Controllers
var bikeRaksController = require('./app/controllers/bikeraks');
var userController = require('./app/controllers/user');
var authController = require('./app/controllers/auth');

var app = express();

//Connect to DB
mongoose.connect('mongodb://localhost:27017/bikeraks');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json({
}));
 
app.use(passport.initialize());

var router = express.Router();

//ROUTES------------------------------
// Create endpoint handlers for /locations
router.route('/bikeraks')
  .post(authController.isAuthenticated, bikeRaksController.postLocations)
  .get(locationController.getLocations);
 
// Create endpoint handlers for /Locations/:Location_id
router.route('/bikeraks/:location_id')
  .get(authController.isAuthenticated, bikeRaksController.getLocation)
  .put(authController.isAuthenticated, bikeRaksController.putLocation)
  .delete(authController.isAuthenticated, bikeRaksController.deleteLocation);
 
//Create endpoint handlers for /users
router.route('/users')
  .post(userController.postUsers)
  .get(authController.isAuthenticated, userController.getUsers);
 
//Create endpoint handler for authenticating users
router.route('/authenticate')
  .post(userController.authenticateUser);





app.use('/api', router);

app.get('/', function (req, res) {
    res.send('Test');
});

app.listen(process.env.PORT || 3000);

console.log('---BIKERAKS');
console.log('---NODE.JS SERVER');
console.log('---RUNNING ON PORT 3000');